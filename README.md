# Awesome RlsBar

(EN)
Welcome to the technical challenge! Thanks for taking the time to go through our
interview process. We'll try to keep it brief.

For this challenge we will ask you to create a few screens and components on a React project 
for a simple Cocktail wiki, fetching data from a given JSON API.
We provide you with some base code, and will ask you to implement some features on top of that.
You must use the code patterns present on the provided screens 
(React.Component, lifecycle methods, function components, etc.).
On the screens and components that you will develop you may use your favorite pattern.

To do:

1. Page with a list of the alphabet letters (A-Z). By clicking on a letter, it should search drinks started by this letter.
2. Individual cocktail page
   - Ingredients should have a link to an individual page
   - Text input to filter the list of ingredients
3. Individual category page that shows a list of cocktails
   - It should be possible to sort the list alphabetically

### CocktailDB API
https://www.thecocktaildb.com/api.php


This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

### Development setup

You must have node.js and NPM installed

- `npm install` or `yarn install`
- `npm start` or `yarn start`


## Available Scripts

In the project directory, you can run:

### `yarn start` or `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).