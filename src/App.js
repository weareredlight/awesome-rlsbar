import React from 'react';
import { BrowserRouter, Route, Switch } from "react-router-dom";

import Container from 'react-bootstrap/Container';

import Home from './pages/Home';
import Todo from './pages/Todo';
import Search from './pages/Search';
import Cocktails from './pages/Cocktails';
import Header from './components/Header';


function App() {
  return (
    <BrowserRouter>
      <Container className="p-3">
        <Header />
        <Switch>
          <Route path="/cocktails">
            <Cocktails />
          </Route>
          <Route path="/search">
            <Search />
          </Route>
          <Route path="/todo">
            <Todo />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
      </Container>
    </BrowserRouter>
  );
}

export default App;
