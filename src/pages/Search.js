import React from 'react';
import Jumbotron from 'react-bootstrap/Jumbotron';
import Nav from 'react-bootstrap/Nav';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { withRouter } from 'react-router-dom';


class Search extends React.PureComponent {
  componentDidMount() {
    this.processSearchParam();
  }

  processSearchParam() {
    const { location } = this.props;
    const qs = new URLSearchParams(location.search);
    const searchQuery = qs.get('q');

    if (searchQuery) {
      // TODO: query api and display results
      console.log('searching for ' + searchQuery)
    }
  }

  render() {
    return (
      <Row>
        <Col sm={8}>
          <Jumbotron>
            <h1 className="header">RedLight Bar</h1>
          </Jumbotron>
        </Col>
        <Col sm={4}>
          <Nav className="flex-column">

          </Nav>
        </Col>
      </Row>
    );
  }
}

export default withRouter(Search);